package org.uu.nl.net2apl.core.plan.builtin;

import org.uu.nl.net2apl.core.agent.State;
import org.uu.nl.net2apl.core.agent.Norm;
import org.uu.nl.net2apl.core.agent.NormContext;
import org.uu.nl.net2apl.core.agent.ActionContext;
import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.properties.PropertyBase;
import org.uu.nl.net2apl.core.properties.PropertyToPlanInterface;
import org.uu.nl.net2apl.core.properties.Property;

import java.util.List;
import java.util.ArrayList;

/**
 * An evaluable plan is a plan that the agent will analyse and evaluate during execution. First, the plan
 * will be analysed, after which its utility will be calculated based on the agent's properties and
 * corresponding weights. The EvaluablePlan class is used when the programmer wants just a single plan
 * for a goal to be evaluated for execution.
 * 
 * @author Giannina Klijnsma
 */
public abstract class EvaluablePlan extends EvaluablePlanBasis {
    private PropertyToPlanInterface propertyToPlanInterface;
    private List<State> states;
    private List<Norm> obeyedNorms;
    private List<Norm> violatedNorms;
    private List<Norm> detachedNorms;

    public EvaluablePlan(){
        this.propertyToPlanInterface = new PropertyToPlanInterface(this);
        this.states = new ArrayList<State>();
        this.obeyedNorms = new ArrayList<Norm>();
        this.violatedNorms = new ArrayList<Norm>();
        this.detachedNorms = new ArrayList<Norm>();
    }

    /**
     * @return The list of states the agent believes to be visited
     */
    public List<State> getStates(){
        return states;
    }   

    /**
     * @return The list of norms the agent believes to be obeyed in the plan
     */
    public List<Norm> getObeyedNorms(){
        return obeyedNorms;
    }

    /**
     * @return The list of norms the agent believes to be violated in the plan
     */
    public List<Norm> getViolatedNorms(){
        return violatedNorms;
    }

    /**
     * @return The list of norms the agent believes to be detached the plan
     */
    public List<Norm> getDetachedNorms(){
        return detachedNorms;
    }

    @Override
    public final Double evaluate(final PlanToAgentInterface planInterface, final AgentContextInterface contextInterface) {
        // Analyse the plan        
        analyse(planInterface);

        // Get the agent's property base, calculate the utility of the analysation
        PropertyBase propertyBase = planInterface.getPropertyBase();
        double utility = 0;
        for(Property property : propertyBase.getProperties()){
            double propertyValue = property.calculate(this.propertyToPlanInterface, contextInterface);
            double propertyWeight = propertyBase.getWeight(property.getName());
            if(propertyValue < -1 || propertyValue > 1){
                throw new IllegalArgumentException("Property has an invalid value. Must be between -1 and 1.");
            }
            double propertyFinal = propertyValue * propertyWeight;
            utility += propertyFinal;
        }
        Reset();        
        return utility;
    }    

    /**
     * Resets the analysation
     */
    private final void Reset(){
        this.states = new ArrayList<State>();
        this.obeyedNorms = new ArrayList<Norm>();
        this.violatedNorms = new ArrayList<Norm>();
    }
    
    /**
     * Analyses the plan. From this we should obtain the list of states the agent
     * believes will be visited, and the list of norms it believes will be detached,
     * obeyed, and violated. 
     * @param planInterface
     */
    private void analyse(final PlanToAgentInterface planInterface){
        List<Object> actionIDs = getActionIDs(planInterface);
        ActionContext actionContext = planInterface.getContext(ActionContext.class);
        State state = planInterface.getBeliefState();
        states.add(state.copy());

        for(Object actionID : actionIDs){        
            // Check if any norms are obeyed/violated if we perform this action in this state    
            checkNormConformity(planInterface, state, actionID);
            // Get the next state by analysing the action on the state, add it to the list and continue analysing
            State nextState = actionContext.analyseAction(state, actionID);
            states.add(nextState.copy());
            state = nextState;
        }        

        // Check if any norms are detached in the last state
        checkNormConformity(planInterface, state, null);
    }

    /**
     * Checks if any norms are detached in the given state, and whether they are obeyed or not given the
     * action ID, and adds them to the relevant lists.
     * @param planInterface
     * @param state The state which needs to be checked for detached norms
     * @param actionID The ID of the action that will be performed in this state
     */
    private void checkNormConformity(final PlanToAgentInterface planInterface, State state, Object actionID){
        NormContext normContext = planInterface.getContext(NormContext.class);
        if(normContext != null){
            for(Norm norm : normContext.getNorms()){
                if(norm.isDetached(state)){
                    detachedNorms.add(norm);
                    // Only check for norm conformity if we are performing an action in this state
                    if(actionID != null){
                        if(norm.isViolated(actionID)){
                            violatedNorms.add(norm);
                        }
                        else{
                            obeyedNorms.add(norm);
                        }
                    }
                }
            }
        }
    }    
    
    /**
     * Get the list of action IDs that the agent will use to analyse the plan with.
     */
    public abstract List<Object> getActionIDs(final PlanToAgentInterface planInterface);
}