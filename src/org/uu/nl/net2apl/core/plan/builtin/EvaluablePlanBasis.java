package org.uu.nl.net2apl.core.plan.builtin;

import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.Goal;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.plan.Plan;

/**
 * This class acts as a base class for the EvaluablePlan and EvaluabePlanOptions classes.
 * The EvaluablePlan class is used by the programmer to implement a single plan for a goal
 * that must be evaluated for execution, and the EvaluablePlanOptions class can be used to
 * evaluate a set of plans for a single goal. Only plans that are instances of this class
 * will be evaluated in the deliberation cycle.
 * 
 * @author Giannina Klijnsma
 */
public abstract class EvaluablePlanBasis extends Plan {
    private boolean evaluated;
    
    /**
     * Finds the utility of the plan
     * @param planInterface
     * @param contextInterface
     * @return The final utility of the plan
     */
    public abstract Double evaluate(final PlanToAgentInterface planInterface, final AgentContextInterface contextInterface);

    /**    
     * @return Returns true if the plan has evaluated. 
     */
    public final boolean isEvaluated(){ return this.evaluated; }
    	
	/**
	 * Setting the argument to true will result in this plan being no longer evaluated for 
     * execution. Setting the argument to false will ensure that the plan will be evaluated
	 * again during the next deliberation cycle if it is not yet finished
	 * @param evaluated The value to set the boolean to
	 */
	protected final void setEvaluated(final boolean evaluated){ 
		this.evaluated = evaluated;
	}  	

    /**
     * @return Returns the goal of the plan
     */    
    public Goal getGoal(){
        return this.goal;
    }
}