package org.uu.nl.net2apl.core.plan.builtin;

import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.plan.PlanExecutionError;

import java.util.List;

/**
 * The EvaluablePlanOptions class can be used by the programmer to evaluate
 * a set of plans for a goal for execution. This class will return the utility
 * of the plan with the highest preference, and execute this best plan if the
 * deliberation step has not dropped it. 
 * 
 * @author Giannina Klijnsma
 */
public abstract class EvaluablePlanOptions extends EvaluablePlanBasis {
    private EvaluablePlan bestPlan;   

    @Override
    public final Double evaluate(final PlanToAgentInterface planInterface, final AgentContextInterface contextInterface) {
        double bestPreference = -999;
        for(EvaluablePlan plan : getPlanOptions()){
            double preference  = plan.evaluate(planInterface, contextInterface);
            if(preference > bestPreference){
                this.bestPlan = plan;
                bestPreference = preference;
            }
        }
        return bestPreference;
    }    

    @Override
    public final void execute(final PlanToAgentInterface planInterface) throws PlanExecutionError{
        this.bestPlan.execute(planInterface);
        if(this.bestPlan.isFinished()){
            this.goal.setPursued(false);
            setFinished(true);
        }
    }

    /**
     * @return The list of plan options that will be evaluated for execution.
     */
    public abstract List<EvaluablePlan> getPlanOptions();
}