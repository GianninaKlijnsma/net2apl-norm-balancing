package org.uu.nl.net2apl.core.fipa.ams;

public enum ActionStatus {
	/**
	 * 
	 */
	SUCCESS,
	/**
	 * 
	 */
	DUPLICATE,
	/**
	 * 
	 */
	ACCESS, //unauthorized access
	/**
	 * 
	 */
	INVALID,
	/**
	 * 
	 */
	NOT_FOUND,
}
