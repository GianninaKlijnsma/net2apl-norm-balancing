package org.uu.nl.net2apl.core.defaults.deliberationsteps;

import org.uu.nl.net2apl.core.plan.builtin.EvaluablePlanBasis;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.agent.Agent;
import org.uu.nl.net2apl.core.agent.Goal;
import org.uu.nl.net2apl.core.deliberation.DeliberationStepException;

/**
 * Deliberation step for evaluating the current plans of the agent.
 * 
 * @author Giannina Klijnsma
 */
public final class EvaluatePlans extends DefaultDeliberationStep { 
	
	public EvaluatePlans(final Agent agent){
		super(agent);
	}
	
	/** 
     * This steps evaluates each plan by going through each of the agent's plans.
     * If the value of the resulting evaluating is negative, we drop the plan.
     * Else, we execute it.
     */
	@Override
	public final void execute() throws DeliberationStepException {
		for(Plan plan : super.agent.getPlans()){ 			
			if(plan instanceof EvaluablePlanBasis && !((EvaluablePlanBasis)plan).isEvaluated()){
				if(super.agent.evaluatePlan((EvaluablePlanBasis)plan) < 0){
					super.agent.removePlan(plan);
					Goal goal = ((EvaluablePlanBasis)plan).getGoal();
					super.agent.dropGoal(goal);
				}
			}
		}
	}
}
