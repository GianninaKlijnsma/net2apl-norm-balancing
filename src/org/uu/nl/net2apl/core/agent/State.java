package org.uu.nl.net2apl.core.agent;

 /**
  * A State keeps track of a list of facts that hold in the context of an agent. It is up to the programmer
  * to define what facts from a context are relevant. The relevant facts should be the ones that are needed
  * to evaluate a plan. Furthermore, we should be able to make a deep copy of the state.
  *
  * @author Giannina Klijnsma
  */

public interface State {
    /**
     * Gets the relevant information for the state from the context
     * @param contextInterface The contexts from which the information must be gathered from
     */
    public void initialise(AgentContextInterface contextInterface);

    /**     
     * @return A deep copy of the current state
     */
    public State copy();
}
