package org.uu.nl.net2apl.core.agent;

import java.util.List;
import java.util.ArrayList;

/**
 * The NormContext acts as the agent's norm base, which holds the list
 * of norms that the agent can conform with or violate during execution.
 * 
 * @author Giannina Klijnsma
 */
public class NormContext implements Context {
    protected List<Norm> norms;

    public NormContext(){
        this.norms = new ArrayList<Norm>();
    }

    /**    
     * @return Get the list of norms from the context
     */
    public List<Norm> getNorms(){
        return this.norms;
    }

    /**
     * Add all the norms in the list to the list of norms in the context
     * @param norms The list of norms to be added
     */
    public void addNorms(List<Norm> norms){
        this.norms.addAll(norms);
    }

    /**
     * Add a single norm to the list of norms
     * @param norm The norm to be added
     */
    public void addNorm(Norm norm){
        this.norms.add(norm);
    }
}