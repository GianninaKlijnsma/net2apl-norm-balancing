package org.uu.nl.net2apl.core.agent;

/**
 * An type of context that defines the beliefs over the effects of actions on the agent's contexts.
 * 
 * @author Giannina Klijnsma
 */
public interface ActionContext extends Context {
    /**
     * Analyse a given action on a given state, and returns the state that the agent believes would result from it
     * @param state The initial state that we are analysing
     * @param action The identifier of the action that we are analysing
     * @return The state that the agent believes would be achieved if the given action would be executed on the given state
     */
    public State analyseAction(State state, Object actionID);
    
}