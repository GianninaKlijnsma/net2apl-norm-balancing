package org.uu.nl.net2apl.core.agent;

/**
 * A Norm implements a kind of rule that the agent can chose to conform with during execution.
 * If a norm is violated, the agent should be sanctioned. 
 * 
 * @author Giannina Klijnsma
 */
public interface Norm {    
    /**
     * @return The sanction of the norm
     */
    public Object getSanction();

    /**
     * Checks if a norm is detached in a given state
     * @param state The state which must be checked
     * @return Returns true if the norm is detached in the given state
     */
    public boolean isDetached(State state);

      /**
     * Checks if a norm is violated given an action
     * @param actionID The identifier of the action that must be checked
     * @return Returns true if the norm is violated given the action identifier
     */
    public boolean isViolated(Object actionID);
}