package org.uu.nl.net2apl.core.agent;

/**
 * A trigger is anything that can trigger a plan for an agent (as specified by 
 * the plan's scheme). 
 * 
 * @author Bas Testerink
 */
public interface Trigger {} 