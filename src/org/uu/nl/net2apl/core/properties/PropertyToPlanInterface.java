package org.uu.nl.net2apl.core.properties;

import org.uu.nl.net2apl.core.agent.State;
import org.uu.nl.net2apl.core.agent.Norm;
import org.uu.nl.net2apl.core.plan.builtin.EvaluablePlan;

import java.util.List;

/**
 * Exposes all functionalities from an evaluable plan that the calculation of a
 * property might need/is allowed to call upon, according to 2APL standards.
 * 
 * @author Giannina Klijnsma
 */
public final class PropertyToPlanInterface {
	/** The plan that is exposed by this interface. */
	private final EvaluablePlan plan;
	
	public PropertyToPlanInterface(final EvaluablePlan plan){
		this.plan = plan;
    }

    /**
     * @return The states the agent believes to be visited in the plan after analysing it
     */
    public List<State> getStates() { return this.plan.getStates(); }

    /**
     * @return Gets the first state that the agent believes will be visited in the plan
     */
    public State getFirstState() { return getStates().get(0); }

    /**
     * @return Gets the last state that the agent believes will be visited in the plan
     */
    public State getLastState() { return getStates().get(getStates().size() - 1); }

    /**
     * @return The norms the agent believes to be detached and violated in the plan after analysing it
     */
    public List<Norm> getViolatedNorms() { return this.plan.getViolatedNorms(); }

    /**
     * @return The norms the agent believes to be detached and obeyed in the plan after analysing it
     */
    public List<Norm> getObeyedNorms() { return this.plan.getObeyedNorms(); }

    /**
     * @return The norms the agent believes to be detached in the plan after analysing it
     */
    public List<Norm> getDetachedNorms() { return this.plan.getDetachedNorms(); }
}