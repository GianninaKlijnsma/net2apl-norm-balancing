package org.uu.nl.net2apl.core.properties;

import java.util.List;
import java.util.Map;

/**
 * Implements the list of properties that the agent will use to evaluate plans. 
 * Also holds the weight the agent gives to each property. 
 *  
 * @author Giannina Klijnsma
 */
public final class PropertyBase {   
    private List<Property> properties;
    private Map<String, Double> weights;

    public PropertyBase(final List<Property> properties, final Map<String, Double> weights){
        this.properties = properties;
        this.weights = weights;
    }

    /**
     * @return Get the list of properties
     */
    public final List<Property> getProperties(){
        return this.properties;
    }

    /**
     * Set the weight of a given property. The weight must be within [-1, +1] 
     * @param name The name of the property
     * @param value The value of the weight. Must be within [-1, +1]
     */
    public void setWeight(String name, double value){
        if(value < -1 || value > 1){
            throw new IllegalArgumentException("Property weight has an invalid value, must be between -1 and 1.");
        }
        this.weights.put(name, value);
    }

    /**
     * Get the weight of the property with the given name
     * @param name The name of the property
     * @return The weight of the property
     */
    public double getWeight(String name){
        return this.weights.get(name);
    }
}