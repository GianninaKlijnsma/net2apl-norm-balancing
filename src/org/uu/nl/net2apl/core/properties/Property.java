package org.uu.nl.net2apl.core.properties;

import org.uu.nl.net2apl.core.agent.AgentContextInterface;

/**
 * An abstract class that defines a property of a plan by a name, and a way to calculate it.
 * The agent uses Properties to calculate the utility of a plan.
 * 
 * @author Giannina Klijnsma
 */

public interface Property {
    /**    
     * @return The name of this property
     */
    public String getName();

    /**
     * Defines the calculation of this property. 
     * @param propertyToPlanInterface Exposes variables obtained during analysation of a plan
     * @param contextInterface Exposes the agent's contexts
     * @return The value of this property, which is between [-1, +1]
     */
    public double calculate(PropertyToPlanInterface propertyToPlanInterface, AgentContextInterface contextInterface);
}